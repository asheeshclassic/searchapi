/*
 * File used to search topics based on parameters
 * @Author: Asheesh Sharma
*/
var map;
//initialize Map by this function
function initMap(topic, radiusVal) {
	//Hide point of interests
	var noPoi =[{
		"featureType": "poi",
		"elementType": "labels",
		"stylers": [
			{
				"visibility": "off"
			}
		]
	}];
	//code added for autocomplete
	var input = document.getElementById('autocomplete');
	var autocomplete = new google.maps.places.Autocomplete(input);
	//Add place change listener to get lat and long
	autocomplete.addListener('place_changed', function() {
		var placename = autocomplete.getPlace();
		var lati 	= 	placename.geometry.location.lat(),
			longi 	= 	placename.geometry.location.lng();
		$('#latitude').val(lati);
		$('#longitude').val(longi);       
	});
	var latitude	=	$('#latitude').val();
	var longitude	=	$('#longitude').val();
	if(latitude == '' && longitude == '') { 
		latitude 	= 	-33.866;
		longitude 	= 	151.196;
	}	
	// Create the map.
	var selectedplace = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
	map = new google.maps.Map(document.getElementById('map'), {
		center: selectedplace,
		zoom: 17,
		styles: noPoi
	});
	// a new Info Window is created
	infoWindow = new google.maps.InfoWindow();

	// Event that closes the Info Window with a click on the map
	google.maps.event.addListener(map, 'click', function() {
		infoWindow.close();
	});
	   
	// Create the places service.
	var service = new google.maps.places.PlacesService(map);
	var getNextPage = null;
	var moreButton = document.getElementById('more');
	moreButton.onclick = function() {
		moreButton.disabled = true;
		if (getNextPage) getNextPage();
	};
	if(radiusVal) {
		// Perform a nearby search.
		var request = {
			location: selectedplace,
			radius: radiusVal,
			keyword: topic,
			query: topic
		};	
		service.nearbySearch(request,
		function(results, status, pagination) { 
			var x = document.getElementById("more");
			if(status === 'ZERO_RESULTS') {
				x.style.visibility = "hidden";			
				var placesList = document.getElementById('places');
				var myhtml = '<div class="alert alert-danger" role="alert">No Result Found</div>';
				document.getElementById("places").innerHTML = myhtml;
			}
			if (status !== 'OK') return;
				x.style.visibility = "visible";
				createMarkers(results);
				moreButton.disabled = !pagination.hasNextPage;
				getNextPage = pagination.hasNextPage && function() {
				pagination.nextPage();
			};
		});	
	}	
}	
/*
 * Function called to get all Markers
 * @params: places
 * @Author: Asheesh Sharma
 */
function createMarkers(places) {
	var bounds = new google.maps.LatLngBounds();
	var placesList = document.getElementById('places');		
	for (var i = 0, place; place = places[i]; i++) {			
		var position 	= place.geometry.location;
		var name 		= place.name;
		var vicinity 	= place.vicinity;
		var rating 		= place.rating;
		createMarker(position, name, vicinity, rating);			
		var li 			= document.createElement('li');
		var newSpan 	= document.createElement('span');
		li.className 	= "list-group-item";
		newSpan.className 	= "vicinity";
		li.textContent 	= place.name;
		newSpan.textContent 	= place.vicinity;
		li.appendChild(newSpan);
		placesList.appendChild(li);
		bounds.extend(place.geometry.location);
	}
	map.fitBounds(bounds);		
}
/*
 * This function creates each marker and it sets their Info Window content
 * @params: latlng, name
 * @Author: Asheesh Sharma
 */	
function createMarker(latlng, name, vicinity, rating){
	var image = {
		url: "images/3.png"
	};
	var marker = new google.maps.Marker({
	  map: map,
	  icon: image,
	  position: latlng,
	  title: name
	});
	// This event expects a click on a marker
	// When this event is fired the Info Window content is created
	// and the Info Window is opened.
	google.maps.event.addListener(marker, 'click', function() {	  		
		var iwContent =	'<div id="iw_container" class="map-popup"><div class="info-data"><div class="iw_title"><a href="javascript:void(0);">'+name+'</a></div></div><figure><br />'+vicinity+'</figure></div>';
		// including content to the Info Window.
		infoWindow.setContent(iwContent);
		// opening the Info Window in the current map and at the current marker location.
		infoWindow.open(map, marker);
	});	
}
$(function() {
	$( "#demo" ).customScroll({ scrollbarWidth: 10 });
});
//Bind search button and enter click	
$(document).ready(function() {	
	$("body").keyup(function(event) {
		if (event.which === 13) {
			$("#searchbtn").trigger('click');
		}
	});	
	$('body').on('click', '#searchbtn', function() {		
		var topic 			= 	$('#topic').val();
		var radius 			= 	$('.distanceVal').html();
		if(topic == '') {
			swal({				
				title: "",
				text: "Please enter the topic",
				type: "warning",
				showCancelButton: false,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Ok",
				closeOnConfirm: true
			},
			function(){
				$('#topic').focus();
			});
			return;
		}
		document.getElementById("places").innerHTML = "";
		//call Map initialize function
		initMap(topic, radius);
	});						
});